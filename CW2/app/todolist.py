from flask import  render_template , request , redirect , session
from .forms import userdetails, itemdetails , changep
from flask_admin.contrib.sqla import ModelView
from app import app , db  , models , admin
from .models import User,Item , Receipt , Transcriptitem
import datetime
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import logging

logging.basicConfig(filename='demo.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s %(name)s : %(message)s')

login_manager = LoginManager()
login_manager.init_app(app)

admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Item, db.session))
admin.add_view(ModelView(Receipt, db.session))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

#  Homepage with initial create task button that redirects to create task page
@app.route('/',methods=["POST","GET"])
def index():
    if current_user.is_authenticated:
        return redirect('/Shop')
    form = userdetails()
    users = models.User.query.all()
    if form.validate_on_submit():
        for x in users:
            if form.username.data == 'Admin' and form.password.data == 'Admin':
                app.logger.info('Admin logged in')
                login_user(x)
                return render_template('Adminpage.html')
            elif form.username.data == x.username and form.password.data == x.password and request.form.get('rememberme'):
                login_user(x, remember=True)
                app.logger.info('User logged in')
                return redirect('/Shop')
            elif form.username.data == x.username and form.password.data == x.password:
                login_user(x)
                app.logger.info('User logged in')
                return redirect('/Shop')
            else: 
                app.logger.info('Failed login check')
    return render_template('Homepage.html',
                            form=form,
                            title = 'Home')

@app.route('/Stock',methods=["POST","GET"])
def Stock():
    form = itemdetails()
    if form.validate_on_submit():
        newStock = models.Item(itemname=form.itemname.data,itemtype=form.itemtype.data,itemprice=form.itemprice.data,Quantity = 1)
        db.session.add(newStock)
        db.session.commit()
        app.logger.info('Stock added by Admin')
    return render_template('Stock.html',
                            form=form,
                            allitems = models.Item.query.all(),
                            title = 'Add or Remove Items')
#  Create Account
@app.route("/Create", methods=["POST","GET"])
def create():
    form = userdetails()
    if form.validate_on_submit():  #  Check fields arent blank
        newuser = models.User(username=form.username.data,password=form.password.data)
        db.session.add(newuser)                 #  Add user to database
        db.session.commit()
        app.logger.info('User Account creation successful')    
        return redirect('/')
    app.logger.info('Unsuccessful Account creation')
    return render_template('index.html',
                            form=form,
                            title = 'Create an account')

#  View page with products wheter a user is logged in or not
@app.route('/Shop', methods=["POST","GET"])
def completed():
    if current_user.is_authenticated:
        app.logger.info('User is logged in on product page')
        return render_template('loggedinhome.html',
                                allitems = models.Item.query.all(),
                                title = 'Shop')
    app.logger.info('User NOT logged in on product page')
    return render_template('notloggedinhome.html',
                             allitems = models.Item.query.all(),
                             title = 'Shop')

@app.route('/Account', methods=["POST","GET"])
@login_required
def account():
    transcripts = current_user.transcript
    form = changep()
    if form.validate_on_submit():  #  Check fields arent blank
        if form.oldpassword.data == current_user.password and form.newpassword.data == form.newpassword2.data:
            app.logger.info('USER PASSWORD CHANGED')
            current_user.password = form.newpassword.data
            db.session.commit()
            return redirect('/')
        app.logger.info('Wrong password info for user')
    return render_template('account.html',
                             form=form,
                             orders = transcripts,
                             title = 'Shop')

@app.route('/logout', methods=["POST","GET"])
def logout():
    logout_user()
    app.logger.info('User logged out')
    return redirect('/Shop')

# Add item to user 'basket' by link
@app.route('/addtobasket/<int:id>')
def addtobasket(id):
    if current_user.is_authenticated:
        additemtob = models.Item.query.filter_by(id=id).first()
        for uss in current_user.itemsthisuser:
            if uss.id == additemtob.id:
                uss.Quantity = uss.Quantity + 1
                db.session.commit()
                app.logger.info('ITEM QUANTITY incremented : Variable value changed, check correct value')
                return redirect('/Shop')
        current_user.itemsthisuser.append(additemtob)
        app.logger.info('ADDTOBASKET : Check object is added to database ONCE, not multiple times')
        db.session.commit()
        return redirect('/Shop')
    app.logger.info('GUEST has tried to accessed the page, redirecting to login')
    return redirect('/')

# Admin can delete stock
@app.route('/deletestock/<int:id>')
def deletestock(id):
    models.Item.query.filter_by(id=id).delete()
    db.session.commit()
    app.logger.info('Stock deleted : Check database for stock to see if its actually removed')
    return redirect('/Stock')
    
# User Basket page
@app.route('/Basket')
def basket():
    if current_user.is_authenticated:
        totalprice = 0.0
        for uss in current_user.itemsthisuser:
            totalprice = totalprice + uss.itemprice*uss.Quantity
            app.logger.debug('TOTAL PRICE OF BASKET CHANGED :  check correct Variable value')
        return render_template('basket.html',
                                usernow = current_user,
                                title = 'Basket',
                                total=totalprice)
    app.logger.info('GUEST tried to access basket, redirecting to login page')
    return redirect('/')

# Buy button in user basket page, makes a transripts and clears user-item database
@app.route('/bought')
def bought():
    newreceipt=models.Receipt(time=datetime.datetime.utcnow())
    notempty = 0
    for uss in current_user.itemsthisuser:
        notempty = 1
        newtranscriptorder = models.Transcriptitem(itemname=uss.itemname,itemtype=uss.itemtype,itemprice=uss.itemprice,Quantity = uss.Quantity)
        db.session.add(newtranscriptorder)
        newreceipt.orders.append(newtranscriptorder)
        uss.Quantity=1
    for uss in current_user.itemsthisuser:
        current_user.itemsthisuser.clear(uss)
        app.logger.info('CLEARING USER ITEMS after buying : check it is empty')
    if notempty == 1:
        db.session.add(newreceipt)
        current_user.transcript.append(newreceipt)
        app.logger.info('ADDED TRANSCRIPT : Transcript should not be added if it is empty')
        db.session.commit()
    return redirect('/Basket')

# Reduces Quantity value for item by one in user, if less than one then removes from user database
@app.route('/removefrombasket/<int:id>')
def removefrombasket(id):
    additemtob = models.Item.query.filter_by(id=id).first()
    for uss in current_user.itemsthisuser:
        if additemtob == uss:
            uss.Quantity = uss.Quantity -1
            if uss.Quantity < 1:
                uss.Quantity = 1
                current_user.itemsthisuser.remove(uss)
                db.session.commit()
                return redirect('/Basket')
    db.session.commit()
    app.logger.info('Stock deleted : Check database for stock to see if its actually removed')
    return redirect('/Basket')

