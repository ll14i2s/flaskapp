$(document).ready(function() {

    $(".addbbutton").on("submit", function() {
        $.ajax({

            // The data, which is now most commonly formatted using JSON because of its
            // simplicity and is native to JavaScript.
            data: {
                'username': $('#username').val(),
                'password': $('#password').val()
            },
            type : 'POST',
            url : '/'
            success: function(data){
                console.log('success',data);
            }
        }),
          // The function which will be triggered if any error occurs.
        done(function(data){
            $('#erroralert').text(data.error).show()
        });
    });
});
