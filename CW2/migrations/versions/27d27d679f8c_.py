"""empty message

Revision ID: 27d27d679f8c
Revises: 
Create Date: 2021-01-20 13:02:08.058052

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '27d27d679f8c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('item',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('itemname', sa.String(length=100), nullable=True),
    sa.Column('itemtype', sa.String(length=30), nullable=True),
    sa.Column('itemprice', sa.Float(), nullable=True),
    sa.Column('Quantity', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('savetasks',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('task', sa.String(length=100), nullable=True),
    sa.Column('desc', sa.String(length=1000), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.Column('status', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('transcriptitem',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('itemname', sa.String(length=100), nullable=True),
    sa.Column('itemtype', sa.String(length=30), nullable=True),
    sa.Column('itemprice', sa.Float(), nullable=True),
    sa.Column('Quantity', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=30), nullable=True),
    sa.Column('password', sa.String(length=30), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('username')
    )
    op.create_table('itemtable',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('item_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['item_id'], ['item.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], )
    )
    op.create_table('receipt',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('userreceipts', sa.Integer(), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['userreceipts'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('receipttable',
    sa.Column('receipt_id', sa.Integer(), nullable=True),
    sa.Column('transcriptitem_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['receipt_id'], ['receipt.id'], ),
    sa.ForeignKeyConstraint(['transcriptitem_id'], ['transcriptitem.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('receipttable')
    op.drop_table('receipt')
    op.drop_table('itemtable')
    op.drop_table('user')
    op.drop_table('transcriptitem')
    op.drop_table('savetasks')
    op.drop_table('item')
    # ### end Alembic commands ###
